﻿using System.Collections.Generic;
using WebApi.Repositorio.Contrato;

namespace WebApi.Servicos.Contratos
{
    public interface IPlanoVooService
    {
        #region Métodos

        int? Inserir(PlanoVooAPI planoVoo);

        int? Alterar(PlanoVooAPI planoVoo);

        int? Excluir(int Id);

        List<PlanoVooAPI> Listar(int aeroportoOrigem, int aeroportoDestino);

        #endregion
    }
}
