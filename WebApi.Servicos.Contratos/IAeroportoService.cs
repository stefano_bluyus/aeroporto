﻿using WebApi.ObjetosNegocio;
using System.Collections.Generic;

namespace WebApi.Servicos.Contratos
{
    public interface IAeroportoService
    {
        #region Métodos

        List<Aeroporto> Listar();

        #endregion
    }
}
