﻿using Ninject.Modules;
using WebApi.Servicos.Contratos;
using WebApi.Servicos.Implementacoes;

namespace WebApi.Servicos.NinjectModules
{

    public class AeroportoServiceModule : NinjectModule
    {
        #region Métodos

        public override void Load()
        {
            Bind<IAeroportoService>().To<AeroportoService>();
        }

        #endregion
    }
}
