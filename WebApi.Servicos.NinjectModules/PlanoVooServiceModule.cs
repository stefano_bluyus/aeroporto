﻿using Ninject.Modules;
using WebApi.Servicos.Contratos;
using WebApi.Servicos.Implementacoes;

namespace WebApi.Servicos.NinjectModules
{

    public class PlanoVooServiceModule : NinjectModule
    {
        #region Métodos

        public override void Load()
        {
            Bind<IPlanoVooService>().To<PlanoVooService>();
        }

        #endregion
    }
}
