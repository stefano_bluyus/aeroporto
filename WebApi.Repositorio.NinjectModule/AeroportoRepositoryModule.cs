﻿using Ninject.Modules;
using WebApi.Repositorio.Contrato;
using WebApi.Repositorio.Implementacao;

namespace WebAPI.Repositorios.NinjectModules.V1
{
    public class AeroportoRepositoryModule : NinjectModule
    {
        #region Métodos

        public override void Load()
        {
            Bind<IAeroportoRepository>().To<AeroportoRepository>();
        }

        #endregion
    }
}
