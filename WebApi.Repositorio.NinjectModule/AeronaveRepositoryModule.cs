﻿using Ninject.Modules;
using WebApi.Repositorio.Contrato;
using WebApi.Repositorio.Implementacao;

namespace WebAPI.Repositorios.NinjectModules.V1
{
    public class AeronaveRepositoryModule : NinjectModule
    {
        #region Métodos

        public override void Load()
        {
            Bind<IAeronaveRepository>().To<AeronaveRepository>();
        }

        #endregion
    }
}