﻿using Ninject.Modules;
using WebApi.Repositorio.Contrato;
using WebApi.Repositorio.Implementacao;

namespace WebAPI.Repositorios.NinjectModules.V1
{
    public class PlanoVooRepositoryModule : NinjectModule
    {
        #region Métodos

        public override void Load()
        {
            Bind<IPlanoVooRepository>().To<PlanoVooRepository>();
        }

        #endregion
    }
}
