﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApi.ObjetosNegocio;
using WebApi.Servicos.Contratos;
using WebApi_Final.Tests.AeroportoTest;

namespace WebApi_Final.Tests
{
    [TestClass]
    public class AeroportoServiceTest: IAeroportoService
    {
        public List<Aeroporto> Listar()
        {
            var aeroportoRepository = new AeroportoRepositoryTest();

            var aeroportoList = new List<Aeroporto>();
            return aeroportoList = aeroportoRepository.Buscar();
        }

        [TestMethod]
        public void TestarSeListarAeroportosRetornaDoisItens()
        {
            var expectedResultSet = new List<Aeroporto>();
            expectedResultSet.Add(new Aeroporto() { Id = 1, Sigla = "VCP", Nome = "Viracopos" });
            expectedResultSet.Add(new Aeroporto() { Id = 2, Sigla = "abc", Nome = "abc" });

            var _aeroportoService = new AeroportoServiceTest();

            var aeroportoResultSet = _aeroportoService.Listar();

            Assert.AreEqual(expectedResultSet.Count, aeroportoResultSet.Count, "Não retornou 2 aeroportos");
        }
    }
}
