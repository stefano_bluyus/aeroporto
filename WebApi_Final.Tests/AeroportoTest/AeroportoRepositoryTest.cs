﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.ObjetosNegocio;
using WebApi.Repositorio.Contrato;

namespace WebApi_Final.Tests.AeroportoTest
{
    [TestClass]
    class AeroportoRepositoryTest : IAeroportoRepository
    {
        public List<Aeroporto> Buscar()
        {
            var aeroportoList = new List<Aeroporto>
            {
                new Aeroporto() { Id = 1, Sigla = "VCP", Nome = "Viracopos" },
                new Aeroporto() { Id = 2, Sigla = "abc", Nome = "abc" }
            };

            return aeroportoList;
        }

        public Aeroporto Buscar(int Id)
        {
            return new Aeroporto() { Id = Id, Sigla = "VCP", Nome = "Viracopos" };
        }

        [TestMethod]
        public void TesteBuscaAeroportoViracopos()
        {
            var expectedResultSet = new Aeroporto() { Id = 1, Sigla = "VCP", Nome = "Viracopos" };

            var _aeroportoRepository = new AeroportoRepositoryTest();
            var aeroportoResultSet = _aeroportoRepository.Buscar(1);

            Assert.AreEqual(expectedResultSet.Sigla, "VCP", "Não retornou o aeroporto correto");
        }

        [TestMethod]
        public void TesteFalhaBuscaAeroportoViracopos()
        {
            var expectedResultSet = new Aeroporto() { Id = 2, Sigla = "abc", Nome = "abc" };

            var _aeroportoRepository = new AeroportoRepositoryTest();
            var aeroportoResultSet = _aeroportoRepository.Buscar(1);

            Assert.AreEqual(expectedResultSet.Sigla, "VCP", "Não retornou o aeroporto correto");
        }
    }
}
