﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.ObjetosNegocio;
using WebApi.Repositorio.Contrato;

namespace WebApi_Final.Tests.AeronaveTest
{
    [TestClass]
    class AeronaveRepositotyTest : IAeronaveRepository
    {
        public int? Buscar(string Tipo, string Matricula)
        {
            return 1;
        }

        public int? Incluir(string Tipo, string Matricula)
        {
            return 1;
        }

        [TestMethod]
        public void TesteBuscarAeronave()
        {
            var expectedResultSet = new Aeronave() { Id = 1, Tipo = "Peq", Matricula = "657556" };

            var _aeronaveRepository = new AeronaveRepositotyTest();
            var ResultSet = _aeronaveRepository.Buscar("Peq", "657556");

            Assert.AreEqual(expectedResultSet.Id, ResultSet, "Não buscou a aeronave");
        }

        [TestMethod]
        public void TesteInserirAeronave()
        {
            var expectedResultSet = new Aeronave() { Id = 1, Tipo = "Peq", Matricula = "657556" };

            var _aeronaveRepository = new AeronaveRepositotyTest();
            var ResultSet = _aeronaveRepository.Incluir("Peq", "657556");

            Assert.AreEqual(expectedResultSet.Id, ResultSet, "Não incluiu a aeronave");
        }
    }
}
