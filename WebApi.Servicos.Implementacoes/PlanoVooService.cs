﻿using System.Collections.Generic;
using WebApi.ObjetosNegocio;
using WebApi.Repositorio.Contrato;
using WebApi.Servicos.Contratos;

namespace WebApi.Servicos.Implementacoes
{
    public class PlanoVooService : IPlanoVooService
    {
        #region Campos

        private readonly IPlanoVooRepository _planoVooRepository;
        private readonly IAeroportoRepository _aeroportoRepository;
        private readonly IAeronaveRepository _aeronaveRepository;

        #endregion

        #region Construtores

        public PlanoVooService(IPlanoVooRepository planoVooRepository,
                               IAeroportoRepository aeroportoRepository,
                               IAeronaveRepository aeronaveRepository)
        {
            _planoVooRepository = planoVooRepository;
            _aeroportoRepository = aeroportoRepository;
            _aeronaveRepository = aeronaveRepository;
        }

        #endregion

        #region Métodos

        private PlanoVoo CriarPlanoVoo(PlanoVooAPI planoVooApi)
        {
            return new PlanoVoo()
            {
                DataHora = planoVooApi.DataHora,
                DestinoId = planoVooApi.Destino_Id,
                OrigemId = planoVooApi.Origem_Id,
                AeronaveId = ObterCodigoAeronave(planoVooApi.AeronaveTipo, planoVooApi.AeronaveMatricula)
            };
        }

        private int ObterCodigoAeronave(string Tipo, string Matricula)
        {
            var aeronave = _aeronaveRepository.Buscar(Tipo, Matricula);

            if (aeronave == 0)
            {
                aeronave = _aeronaveRepository.Incluir(Tipo, Matricula);
            }

            return (int)aeronave;
        }

        public int? Inserir(PlanoVooAPI planoVooApi)
        {
            var plano = CriarPlanoVoo(planoVooApi);
            return _planoVooRepository.Inserir(plano);
        }

        public int? Alterar(PlanoVooAPI planoVooApi)
        {
            var plano = CriarPlanoVoo(planoVooApi);
            plano.Id = planoVooApi.Id;
            return _planoVooRepository.Alterar(plano, plano.Id);
        }

        public int? Excluir(int Id)
        {
            return _planoVooRepository.Excluir(Id);
        }

        public List<PlanoVooAPI> Listar(int aeroportoOrigem, int aeroportoDestino)
        {
            return _planoVooRepository.Listar(aeroportoOrigem, aeroportoDestino);
        }

        #endregion
    }
}
