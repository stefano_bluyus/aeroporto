﻿using System.Collections.Generic;
using WebApi.ObjetosNegocio;
using WebApi.Repositorio.Contrato;
using WebApi.Servicos.Contratos;

namespace WebApi.Servicos.Implementacoes
{
    public class AeroportoService: IAeroportoService
    {
        #region Campos

        private readonly IAeroportoRepository _aeroportoRepository;

        #endregion

        #region Construtores

        public AeroportoService(IAeroportoRepository aeroportoRepository)
        {
            _aeroportoRepository = aeroportoRepository;
        }

        #endregion

        #region Métodos
        public List<Aeroporto> Listar()
        {
            return _aeroportoRepository.Buscar();
        }

        #endregion
    }
}
