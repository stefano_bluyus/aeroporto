﻿using System;

namespace WebApi.ObjetosNegocio
{
    public class PlanoVoo
    {
        public int Id { get; set; }

        public DateTime DataHora { get; set; }

        public Aeronave Aeronave { get; set; }

        public Aeroporto Origem { get; set; }

        public Aeroporto Destino { get; set; }

        public int OrigemId { get; set; }

        public int DestinoId { get; set; }

        public int AeronaveId { get; set; }
    }
}
