﻿namespace WebApi.ObjetosNegocio
{
    public class Aeronave
    {
        public int Id { get; set; }

        public string Tipo { get; set; }

        public string Matricula { get; set; }
    }
}
