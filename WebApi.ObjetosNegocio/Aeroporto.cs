﻿using System;
using System.Collections.Generic;

namespace WebApi.ObjetosNegocio
{
    public class Aeroporto
    {
        public int Id { get; set; }

        public string Sigla { get; set; }

        public string Nome { get; set; }
    }
}
