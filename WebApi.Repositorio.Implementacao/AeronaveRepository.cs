﻿using System.Data;
using System.Linq;
using WebApi.ObjetosNegocio;
using WebApi.Repositorio.Contrato;

namespace WebApi.Repositorio.Implementacao
{
    public class AeronaveRepository: IAeronaveRepository
    {
        #region Construtor

        public AeronaveRepository()
        {
        }

        private WebApiContext db = new WebApiContext();

        #endregion

        public int? Buscar(string Tipo, string Matricula)
        {
            return db.Aeronaves.Where(a => a.Matricula == Matricula && a.Tipo == Tipo).Select(a => a.Id).FirstOrDefault();  
        }

        public int? Incluir(string Tipo, string Matricula)
        {
            db.Aeronaves.Add( 
                new Aeronave(){ Tipo = Tipo,
                                Matricula = Matricula });
            db.SaveChanges();

            return Buscar(Tipo, Matricula);

        }
    }
}
