﻿using WebApi.Repositorio.Contrato;
using WebApi.ObjetosNegocio;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System;

namespace WebApi.Repositorio.Implementacao
{
    public class PlanoVooRepository : IPlanoVooRepository
    {
        #region Construtores

        public PlanoVooRepository()
        {
        }

        private WebApiContext db = new WebApiContext();

        #endregion

        #region Métodos

        public int Buscar(PlanoVoo planoVoo)
        {
            return db.PlanoVoos.Where(p => p.DestinoId == planoVoo.DestinoId &&
                                           p.OrigemId == planoVoo.OrigemId &&
                                           p.DataHora == planoVoo.DataHora &&
                                           p.AeronaveId == planoVoo.AeronaveId)
                               .Select(p => p.Id).FirstOrDefault();
        }

        public PlanoVoo Buscar(int Id)
        {
            return db.PlanoVoos.Where(p => p.Id == Id).FirstOrDefault();
        }

        public int? Inserir(PlanoVoo planoVoo)
        {
            db.PlanoVoos.Add(planoVoo);
            db.SaveChanges();
            return Buscar(planoVoo);
        }

        public int? Alterar(PlanoVoo planoVoo, int Id)
        {
            try
            {
                db.Entry(planoVoo).State = EntityState.Modified;
                db.SaveChanges();
                return Id;
            }
            catch
            {
                return null;

            }
        }

        public int? Excluir(int Id)
        {
            try
            {
                db.PlanoVoos.Remove(Buscar(Id));
                db.SaveChanges();
                return 1;
            }            
            catch
            {
                return null;
            }
        }

        public List<PlanoVooAPI> Listar(int aeroportoOrigem, int aeroportoDestino)
        {
            var query = db.PlanoVoos.Join(db.Aeronaves, plano => plano.AeronaveId, aviao => aviao.Id, (plano, aviao) => new { PlanoVoo = plano, Aviao = aviao })
                                    .Join(db.Aeroportos, p => p.PlanoVoo.OrigemId, aeroOri => aeroOri.Id, (p, aeroOri) => new { PlanoVoo = p, AeroportoOrigem = aeroOri })
                                    .Join(db.Aeroportos, p => p.PlanoVoo.PlanoVoo.DestinoId, aeroDes => aeroDes.Id, (p, aeroDes) => new { PlanoVoo = p, AeroportoDestino = aeroDes });

            if (aeroportoOrigem > 0)
            {
                query = query.Where(a => a.PlanoVoo.AeroportoOrigem.Id == aeroportoOrigem);
            }

            if (aeroportoDestino > 0)
            {
                query = query.Where(a => a.AeroportoDestino.Id == aeroportoDestino);
            }

            var teste = query.Select(pv => new PlanoVooAPI
            {
                Id = pv.PlanoVoo.PlanoVoo.PlanoVoo.Id,
                DataHora = pv.PlanoVoo.PlanoVoo.PlanoVoo.DataHora,
                AeronaveTipo = pv.PlanoVoo.PlanoVoo.Aviao.Tipo,
                AeronaveMatricula = pv.PlanoVoo.PlanoVoo.Aviao.Matricula,
                SiglaAeroportoOrigem = pv.PlanoVoo.AeroportoOrigem.Sigla,
                SiglaAeroportoDestino = pv.AeroportoDestino.Sigla,
                Origem_Id = pv.PlanoVoo.AeroportoOrigem.Id,
                Destino_Id = pv.AeroportoDestino.Id
            }).ToList();               

            return teste;
        }
    }

        #endregion
}
