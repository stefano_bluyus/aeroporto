﻿using System.Collections.Generic;
using System.Data.Entity;
using WebApi.ObjetosNegocio;
using WebApi.Repositorio.Implementacao.Map;

namespace WebApi.Repositorio.Implementacao
{
    public class WebApiContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public WebApiContext() : base("Aeroporto")
        {
            Database.SetInitializer<WebApiContext>(new UniDBInitializer<WebApiContext>());
        }

        private class UniDBInitializer<T> : DropCreateDatabaseIfModelChanges<WebApiContext>
        {
            protected override void Seed(WebApiContext context)
            {
                IList<Aeroporto> aeroportos = new List<Aeroporto>();

                aeroportos.Add(new Aeroporto()
                {
                    Nome = "Guarulhos",
                    Sigla = "GRU",
                    Id = 1
                });

                aeroportos.Add(new Aeroporto()
                {
                    Nome = "Maceió",
                    Sigla = "MCZ",
                    Id = 2
                });

                foreach (var aeroporto in aeroportos)
                {
                    context.Aeroportos.Add(aeroporto);
                }                  

                base.Seed(context);
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AeroportoMap());
            modelBuilder.Configurations.Add(new AeronaveMap());
            modelBuilder.Configurations.Add(new PlanoVooMap());

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<PlanoVoo> PlanoVoos { get; set; }

        public DbSet<Aeroporto> Aeroportos { get; set; }

        public DbSet<Aeronave> Aeronaves { get; set; }
    }
}
