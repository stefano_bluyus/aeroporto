﻿using System.Collections.Generic;
using System.Linq;
using WebApi.ObjetosNegocio;
using WebApi.Repositorio.Contrato;

namespace WebApi.Repositorio.Implementacao
{
    public class AeroportoRepository: IAeroportoRepository
    {
        #region Construtores

        public AeroportoRepository()
        {
        }

        private WebApiContext db = new WebApiContext();

        #endregion

        #region Métodos

        public List<Aeroporto> Buscar()
        {
            var teste = db.Aeroportos.ToList();
            return teste;
        }

        public Aeroporto Buscar(int Id)
        {
            return db.Aeroportos.Where(a => a.Id == Id).FirstOrDefault();
        }

        #endregion
    }
}
