﻿using System.Data.Entity.ModelConfiguration;
using WebApi.ObjetosNegocio;

namespace WebApi.Repositorio.Implementacao.Map
{
    public sealed class AeronaveMap : EntityTypeConfiguration<Aeronave>
    {
        public AeronaveMap()
        {
            HasKey(a => a.Id);
            Property(a => a.Matricula).HasMaxLength(15);
            Property(a => a.Matricula).IsRequired();
            Property(a => a.Tipo).HasMaxLength(15);
            Property(a => a.Tipo).IsRequired();
        }
    }

}
