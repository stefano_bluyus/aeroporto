﻿using System.Data.Entity.ModelConfiguration;
using WebApi.ObjetosNegocio;

namespace WebApi.Repositorio.Implementacao.Map
{
    public sealed class PlanoVooMap : EntityTypeConfiguration<PlanoVoo>
    {
        public PlanoVooMap()
        {
            HasKey(pv => pv.Id);

            HasRequired(x => x.Origem)
            .WithMany()
            .HasForeignKey(x => x.OrigemId)
            .WillCascadeOnDelete(false);

            HasRequired(x => x.Aeronave)
            .WithMany()
            .HasForeignKey(x => x.AeronaveId)
            .WillCascadeOnDelete(false);
        }
    }
}
