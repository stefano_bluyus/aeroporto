﻿using System.Data.Entity.ModelConfiguration;
using WebApi.ObjetosNegocio;

namespace WebApi.Repositorio.Implementacao.Map
{
    public sealed class AeroportoMap : EntityTypeConfiguration<Aeroporto>
    {
        public AeroportoMap()
        {
            HasKey(a => a.Id);
            Property(a => a.Sigla).HasMaxLength(5);
            Property(a => a.Sigla).IsRequired();
            Property(a => a.Nome).HasMaxLength(30);
            Property(a => a.Nome).IsRequired();
        }
    }
}
