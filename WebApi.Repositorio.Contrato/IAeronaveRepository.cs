﻿namespace WebApi.Repositorio.Contrato
{
    public interface IAeronaveRepository
    {
        #region Métodos
        int? Buscar(string Tipo, string Matricula);

        int? Incluir(string Tipo, string Matricula);
        #endregion
    }
}
