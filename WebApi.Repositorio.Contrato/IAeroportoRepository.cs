﻿using WebApi.ObjetosNegocio;
using System.Collections.Generic;

namespace WebApi.Repositorio.Contrato
{
    public interface IAeroportoRepository
    {
        #region Métodos
        List<Aeroporto> Buscar();

        Aeroporto Buscar(int Id);

        #endregion
    }
}
