﻿using System.Collections.Generic;
using WebApi.ObjetosNegocio;

namespace WebApi.Repositorio.Contrato
{
    public interface IPlanoVooRepository
    {
        #region Métodos

        int? Inserir(PlanoVoo planoVoo);

        int? Alterar(PlanoVoo planoVoo, int Id);

        int? Excluir(int Id);        

        List<PlanoVooAPI> Listar(int aeroportoOrigem, int aeroportoDestino);

        #endregion
    }
}
