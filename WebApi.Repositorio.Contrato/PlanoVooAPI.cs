﻿using System;

namespace WebApi.Repositorio.Contrato
{
    public class PlanoVooAPI
    {
        public int Id { get; set; }

        public DateTime DataHora { get; set; }

        public int Origem_Id { get; set; }

        public int Destino_Id { get; set; }

        public string AeronaveMatricula { get; set; }

        public string AeronaveTipo { get; set; }

        public string SiglaAeroportoOrigem { get; set; }

        public string SiglaAeroportoDestino { get; set; }
    }
}
