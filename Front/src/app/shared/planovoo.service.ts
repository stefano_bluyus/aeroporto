import { Injectable } from '@angular/core';
import { Planovoo } from './planovoo.modal';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlanovooService {

  formData: Planovoo;
  list: Planovoo[];

  constructor(private http: HttpClient) { }


  postPlanovoo(formData : Planovoo){
    return this.http.post(environment.apiURL+'/PlanoVoo',formData);
     
   }
 
   refreshList(){ 
    this.http.get(environment.apiURL+'/PlanoVoo')
     .toPromise().then(res => this.list = res as Planovoo[]);
   }
 
   putPlanovoo(formData : Planovoo){
     return this.http.put(environment.apiURL+'/PlanoVoo/'+formData.Id,formData);
      
    }
 
    deletePlanovoo(id : number){
     return this.http.delete(environment.apiURL+'/PlanoVoo/'+id);
    }
}
