export class Planovoo {
    Id: number;
    DataHora: string;
    AeronaveTipo: string;
    AeronaveMatricula: string;
    Destino_Id: number;
    Origem_Id: number;
    SiglaAeroportoOrigem: string;
    SiglaAeroportoDestino: string;
}
