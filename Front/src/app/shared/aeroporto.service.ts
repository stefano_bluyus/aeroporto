import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AeroportoService {

  constructor(private http : HttpClient) { }

  getAeroportoList(){
    return this.http.get(environment.apiURL+'/Aeroporto').toPromise();
   }
}
