import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { PlanosvooComponent } from './planosvoo/planosvoo.component';
import { PlanovooComponent } from './planosvoo/planovoo/planovoo.component';
import { PlanovooListComponent } from './planosvoo/planovoo-list/planovoo-list.component';
import { PlanovooService } from './shared/planovoo.service';

@NgModule({
  declarations: [
    AppComponent,
    PlanosvooComponent,
    PlanovooComponent,
    PlanovooListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [PlanovooService],
  bootstrap: [AppComponent]
})
export class AppModule { }
