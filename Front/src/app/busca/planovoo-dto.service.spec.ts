import { TestBed } from '@angular/core/testing';

import { PlanovooDtoService } from './planovoo-dto.service';

describe('PlanovooDtoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlanovooDtoService = TestBed.get(PlanovooDtoService);
    expect(service).toBeTruthy();
  });
});
