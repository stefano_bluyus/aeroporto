import { Injectable } from '@angular/core';
import { PlanovooDto } from 'src/app/busca/planovoo-dto.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Planovoo } from 'src/app/shared/planovoo.modal';

@Injectable({
  providedIn: 'root'
})
export class PlanovooDtoService {

  list: Planovoo[];
  formData:PlanovooDto;

  constructor(private http: HttpClient) { }

  refreshList(form:PlanovooDto){ 
    this.http.get(environment.apiURL+'/PlanoVoo?AeroportoOrigem='+form.AeroportoOrigem+'&AeroportoDestino='+form.AeroportoDestino)
     .toPromise().then(res => this.list = res as Planovoo[]);
   }
}
