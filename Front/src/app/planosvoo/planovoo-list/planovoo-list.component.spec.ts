import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanovooListComponent } from './planovoo-list.component';

describe('PlanovooListComponent', () => {
  let component: PlanovooListComponent;
  let fixture: ComponentFixture<PlanovooListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanovooListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanovooListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
