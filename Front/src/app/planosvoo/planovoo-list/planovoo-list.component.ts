import { Component, OnInit } from '@angular/core';
import { PlanovooService } from 'src/app/shared/planovoo.service';
import { AeroportoService } from 'src/app/shared/aeroporto.service';
import { PlanovooDtoService } from 'src/app/busca/planovoo-dto.service';
import { ToastrService } from 'ngx-toastr';
import { Planovoo } from 'src/app/shared/planovoo.modal';
import { Aeroporto } from 'src/app/shared/aeroporto.model';
import { NgForm } from '@angular/forms';
import { from } from 'rxjs';

@Component({
  selector: 'app-planovoo-list',
  templateUrl: './planovoo-list.component.html',
  styleUrls: ['./planovoo-list.component.css']
})
export class PlanovooListComponent implements OnInit {
  aeroportoList: Aeroporto[];

  constructor(private service: PlanovooService,
    private aeroportoService: AeroportoService, 
    private planovooservice: PlanovooDtoService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
    this.service.refreshList();
    this.aeroportoService.getAeroportoList().then(res => this.aeroportoList = res as Aeroporto[]);
  }

  resetForm(form?: NgForm){
    if(form!=null)
      form.resetForm();

      this.planovooservice.formData = {
        AeroportoDestino: 0,
        AeroportoOrigem: 0
      };
}

  populateForm(plano: Planovoo) {
    //this.service.formData = Object.assign({}, plano);
    this.service.formData = Object.assign({}, plano);
    
    console.log(plano);
  }

  onDelete(id: number) {
    if (confirm('Deseja excluir esse registro?')) {
      this.service.deletePlanovoo(id).subscribe(res => {
        this.service.refreshList();
        this.toastr.warning('Excluído com sucesso', 'Plano excluído');
      });
    }
  }

  onSubmit(form: NgForm) {
    this.planovooservice.refreshList(form.value);
    }

}
