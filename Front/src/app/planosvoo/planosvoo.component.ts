import { Component, OnInit } from '@angular/core';
import { PlanovooService } from '../shared/planovoo.service';

@Component({
  selector: 'app-planosvoo',
  templateUrl: './planosvoo.component.html',
  styleUrls: ['./planosvoo.component.css']
})
export class PlanosvooComponent implements OnInit {

  constructor(private service: PlanovooService) { 

  }

  ngOnInit() {
    this.service.refreshList();
  }

}
