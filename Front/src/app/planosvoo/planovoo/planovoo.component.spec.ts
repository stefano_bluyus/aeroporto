import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanovooComponent } from './planovoo.component';

describe('PlanovooComponent', () => {
  let component: PlanovooComponent;
  let fixture: ComponentFixture<PlanovooComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanovooComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanovooComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
