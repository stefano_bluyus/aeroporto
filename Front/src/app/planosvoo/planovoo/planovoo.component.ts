import { Component, OnInit } from '@angular/core';
import { PlanovooService } from 'src/app/shared/planovoo.service';
import { AeroportoService } from 'src/app/shared/aeroporto.service';
import { NgForm } from '@angular/forms';
import { Toast, ToastrService } from 'ngx-toastr';
import { Aeroporto } from 'src/app/shared/aeroporto.model';

declare var $: any;

@Component({
  selector: 'app-planovoo',
  templateUrl: './planovoo.component.html',
  styleUrls: ['./planovoo.component.css']
})
export class PlanovooComponent implements OnInit {
  aeroportoList: Aeroporto[];
  isValid: boolean = true;

  constructor(private service: PlanovooService, 
              private aeroportoService: AeroportoService, 
              private toastr : ToastrService) { }

  ngOnInit() 
  {
    this.resetForm();
    this.aeroportoService.getAeroportoList().then(res => this.aeroportoList = res as Aeroporto[]);
  }

  resetForm(form?: NgForm){
    if(form!=null)
      form.resetForm();

      this.service.formData = {
        Id: null,
        Destino_Id: 0,
        Origem_Id: 0,
        AeronaveMatricula: '',
        AeronaveTipo: '',
        DataHora: '',
        SiglaAeroportoDestino: '',
        SiglaAeroportoOrigem: ''
      };
} 

validateForm() {
  this.isValid = true;
  if (this.service.formData.Destino_Id == 0)
    this.isValid = false;
  else if (this.service.formData.Origem_Id == 0)
    this.isValid = false;
    else if (this.service.formData.Origem_Id == this.service.formData.Destino_Id)
    this.isValid = false;  
  return this.isValid;
}


onSubmit(form: NgForm) {
  if (this.validateForm()) {
    if (form.value.Id == null)
    this.insertRecord(form);
  else
    this.updateRecord(form);
  }
}

insertRecord(form: NgForm) {
  this.service.postPlanovoo(form.value).subscribe(res => {
    this.toastr.success('Inserido com sucesso', 'Plano incluído');
    this.resetForm(form);
    this.service.refreshList();
  });
}

updateRecord(form: NgForm) {
  this.service.putPlanovoo(form.value).subscribe(res => {
    this.toastr.info('Atualizado com sucesso', 'Plano alterado');
    this.resetForm(form);
    this.service.refreshList();
  });

}

}
