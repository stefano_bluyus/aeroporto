import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanosvooComponent } from './planosvoo.component';

describe('PlanosvooComponent', () => {
  let component: PlanosvooComponent;
  let fixture: ComponentFixture<PlanosvooComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanosvooComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanosvooComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
