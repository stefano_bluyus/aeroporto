﻿using WebApi.Servicos.Contratos;
using System.Web.Http;

namespace WebApi_Final.Controllers
{
    public class AeroportoController : ApiController
    {
        #region Ações

        [HttpGet]
        public IHttpActionResult GetAeroporto()
        {
            var aeroportoSet = _aeroportoService.Listar();

            return Ok(aeroportoSet);
        }

        #endregion

        #region Campos

        private readonly IAeroportoService _aeroportoService;

        #endregion

        #region Construtores

        public AeroportoController(IAeroportoService aeroportoService)
        {
            _aeroportoService = aeroportoService;
        }

        #endregion
    }
}