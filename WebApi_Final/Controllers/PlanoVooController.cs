﻿using WebApi_Final.Models;
using System;
using System.Web.Http;
using WebApi.Repositorio.Contrato;
using WebApi.Servicos.Contratos;

namespace WebApi_Final.Controllers
{

    public class PlanoVooController: ApiController
    {
        #region Ações

        [HttpPost]
        public IHttpActionResult PostPlanoVoo(PlanoVooApiModel planoVooApiModel)
        {
            if (planoVooApiModel == null ||
                string.IsNullOrWhiteSpace(planoVooApiModel.AeronaveMatricula) ||
                string.IsNullOrWhiteSpace(planoVooApiModel.AeronaveTipo) ||
                string.IsNullOrWhiteSpace(planoVooApiModel.DataHora) ||
                (planoVooApiModel.Destino_Id == 0) ||
                (planoVooApiModel.Origem_Id == 0))
            {
                return BadRequest();
            }

            var planoApi = new PlanoVooAPI
            {
                AeronaveMatricula = planoVooApiModel.AeronaveMatricula,
                AeronaveTipo = planoVooApiModel.AeronaveTipo,
                Destino_Id = planoVooApiModel.Destino_Id,
                Origem_Id = planoVooApiModel.Origem_Id,
                DataHora = DateTime.Parse(planoVooApiModel.DataHora),
            };

            int? planovoo = _planoVooService.Inserir(planoApi);

            if (planovoo != null)
            {
                if (planovoo > 0)
                {
                    return Created("", planovoo);
                }
                else
                {
                    return Conflict();

                }
            }

            return InternalServerError();
        }

        [HttpPut]
        public IHttpActionResult PutPlanoVoo(int id, [FromBody]PlanoVooApiModel planoVooApiModel)
        {
            if (planoVooApiModel == null ||
                string.IsNullOrWhiteSpace(planoVooApiModel.AeronaveMatricula) ||
                string.IsNullOrWhiteSpace(planoVooApiModel.AeronaveTipo) ||
                string.IsNullOrWhiteSpace(planoVooApiModel.DataHora) ||
                (planoVooApiModel.Destino_Id == 0) ||
                (planoVooApiModel.Origem_Id == 0))
            {
                return BadRequest();
            }

            var planovooApi = new PlanoVooAPI
            {
                Id = planoVooApiModel.Id,
                AeronaveMatricula = planoVooApiModel.AeronaveMatricula,
                AeronaveTipo = planoVooApiModel.AeronaveTipo,
                Destino_Id = planoVooApiModel.Destino_Id,
                Origem_Id = planoVooApiModel.Origem_Id,
                DataHora = DateTime.Parse(planoVooApiModel.DataHora),
            };

            int? resultado = _planoVooService.Alterar(planovooApi);

            if (resultado != null)
            {
                if (resultado > 0)
                {
                    return Ok();
                }
                else
                {
                    return Conflict();

                }
            }

            return InternalServerError();
        }

        [HttpGet]
        public IHttpActionResult GetPlanoVoo([FromUri]BuscarPlanoVooApiModel buscaPlanoVooApiModel)
        {
            var planoVooSet = _planoVooService.Listar(buscaPlanoVooApiModel.AeroportoOrigem, buscaPlanoVooApiModel.AeroportoDestino);

            return Ok(planoVooSet);
        }

        [HttpDelete]
        public IHttpActionResult DeletePlanoVoo(int id)
        {
            int? resultado = _planoVooService.Excluir(id);

            if (resultado != null)
            {
                if (resultado > 0)
                {
                    return Ok();
                }
            }

            return InternalServerError();
        }

        #endregion

        #region Campos

        private readonly IPlanoVooService _planoVooService;

        #endregion

        #region Construtores

        public PlanoVooController(IPlanoVooService planoVooService)
        {
            _planoVooService = planoVooService;
        }

        #endregion

    }
}