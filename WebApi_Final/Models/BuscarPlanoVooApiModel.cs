﻿using System;
using System.Runtime.Serialization;

namespace WebApi_Final.Models
{
    public class BuscarPlanoVooApiModel
    {
        #region Propriedades

        public int AeroportoDestino { get; set; }
        public int AeroportoOrigem { get; set; }

        #endregion
    }
}