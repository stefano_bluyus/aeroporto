﻿using System;
using System.Runtime.Serialization;

namespace WebApi_Final.Models
{
    public class PlanoVooApiModel
    {
        #region Propriedades

        public int Id { get; set; }
        public string DataHora { get; set; }
        public string AeronaveTipo { get; set; }
        public string AeronaveMatricula { get; set; }
        public int Destino_Id { get; set; }
        public int Origem_Id { get; set; }

        #endregion
    }
}